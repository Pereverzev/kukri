function validateEmail(email) { 
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

jQuery.fn.validateForm = function () {
	if(this[0].tagName.toLowerCase()!=='form'){
		console.error('element supplied for form validation is not a form')
		return false;
	}
	var $form=this;
	var val_result=true;
	$form.find('input,textarea').not('[type="file"]').each(function(){
		$input=$(this);
		val_result=$input.validateField()&&val_result;
	})
	return val_result;
}

jQuery.fn.validateField=function(){
    var $field=$(this);
    if($field.is("[require]")&&!$field.val().length){
        return $field.addError($field.attr('require'));
    }
    if($field.is("[letters]")){
		var pattern = /^[A-Z,a-z,А-Я,а-я ]+$/;
		if(!pattern.test($field.val())){
			return $field.addError($field.attr('letters'));
		}
	}
	if($field.is("[numbers]")){
		var pattern = /^[0-9 -]+$/;
		if(!pattern.test($field.val())){
			return $field.addError($field.attr('numbers'));
		}
	}
	if($field.is("[email]")){
		if(!validateEmail($field.val())){
            return $field.addError($field.attr('email'));
		}
	}
    if($field.is("[group]")){
        var $last=$field
        $(this).closest('form').find('[group]').each(function(){
            if($(this).attr('group')===$field.attr('group')){
                $last=$(this);
            }
        })
        if($field[0]==$last[0]){
            $last.removeData('group_pass'); //очистить чтобы не мешало при повторной валидации
        }else{
            $last.data('group_pass',true);
        }
    }
    $field.removeClass('error').next('.error_msg').remove();
    return true;
}

jQuery.fn.addError=function(error_text){
    var $element=$(this);
    $element.removeClass('error').next('.error_msg').remove();  //очистить предыдущую ошибку
    if($(this).is("[group]")){
        var $last=$element;
        $(this).closest('form').find('[group]').each(function(){
            if($(this).attr('group')===$element.attr('group')){
                $last=$(this);
            }
        })
        if($element[0]==$last[0]&&$last.data('group_pass')){ //если есть указатель что элемент группы прошел валидацию, то группа не обозначается
            $last.removeData('group_pass'); //очистить чтобы не мешало при повторной валидации
            return true;
        }
        else if($element[0]==$last[0]){          //если это последний элемент, и валидация не пройдена, обозначить все поля группы и вывести ошибку
            $(this).closest('form').find('[group]').each(function(){
                if($(this).attr('group')===$element.attr('group')){
                    $(this).addClass('error');
                }
            })
            $element.after('<span class="error_msg">'+$last.attr('group_err')+'</span>');
            return false;
        }else{
            return true;
        }
    }else{
        $element.addClass('error').after('<span class="error_msg">'+error_text+'</span>');
        return false;
    }
}
$(window).load(function(){
    var carousel=$('.jcarousel').jcarousel();
    $('.control_prev').jcarouselControl({
        target: '-=1'
    });
    $('.control_next').jcarouselControl({
        target: '+=1'
    });
    $('.jcarousel_pagination').on('jcarouselpagination:active', 'a', function() {
        $(this).addClass('active');
    }).on('jcarouselpagination:inactive', 'a', function() {
        $(this).removeClass('active');
    }).jcarouselPagination({
        item: function(page) {
            return '<a href="#' + page + '"></a>';
        },
        carousel: carousel
    });
})
$(document).ready(function(){


    $("select").selecter();

    $('.main_prod_filter .selecter').eq(0).addClass('sort-selecter');/*Класс для селектора "Вид изделия" на Главной */
    $('.main_prod_filter .selecter').eq(1).addClass('price-selecter');/*Класс для селектора "Цена" на Главной */
    $('.private_form-input_wrapper .selecter').eq(0).addClass('day-selecter');/*Класс для селектора "День" на странице Настройки личного кабинета */
    $('.private_form-input_wrapper .selecter').eq(1).addClass('month-selecter');/*Класс для селектора "Месяц" на Настройки личного кабинета */
    $('.private_form-input_wrapper .selecter').eq(2).addClass('year-selecter');/*Класс для селектора "Год изделия" на Настройки личного кабинета */
    $('.price-selecter .scroller .selecter-item').eq(1).addClass('to_top_price');
    $('.price-selecter .scroller .selecter-item').eq(2).addClass('to_bottom_price');

    $('.fancybox').magnificPopup({
        type:'inline',

        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    $(function() {
        var input1 = $( "#start_price" );
        var input2 = $( "#finish_price" );
        var slider = $( "#slider-range" ).slider({
          min: 1000,
          max: 99999,
          range: true,
          values: [input1.val(), input2.val()],
          slide: function( event, ui ) {
            input1.val(ui.values[0]);
            input2.val(ui.values[1]);
          }
        });
        $( "#start_price" ).change(function() {
          slider.slider( "values", 0, this.value );
        });
          $( "#finish_price" ).change(function() {
          slider.slider( "values", 1 , this.value);
        });
    });
    

    

    
	if($('.incrementer').length){
		$('.incrementer').wrap('<div class="incrementer_wrapper"></div>');
		$('.incrementer_wrapper').prepend('<span class="less"></span>').append('<span class="more"></span>')
	}
    
    $('.incrementer_wrapper .less').click(function(){
		var incrementer=$(this).closest('.incrementer_wrapper').find('.incrementer')
		if(incrementer.val()<=1){
			return false;
		}else{
			incrementer.val(parseInt(incrementer.val())-1);
		}
	})
	
	$('.incrementer_wrapper .more').click(function(){
		var incrementer=$(this).closest('.incrementer_wrapper').find('.incrementer')
		incrementer.val(parseInt(incrementer.val())+1);
	})
    
    $('.contact_block_form').submit(function(){
        if($(this).validateForm()){
            console.log('form validated');
            return false;
        }else{
            return false;
        }
    })
    
    $('.shop-menu_cat,.shop-menu_subcat').hover(function(){
        var $this=$(this);
        if($this.find('.shop-submenu').length){
            $this.addClass('active').find('.shop-submenu').show().stop().animate({'opacity':1},300);
        }
    },function(){
        var $this=$(this);
        setTimeout(function(){
            $this.removeClass('active').find('.shop-submenu').stop().animate({'opacity':0},{'duration':0,'complete':function(){
                $(this).hide();
            }})
        },300)
    })
    
    $('.submenu_close').click(function(){
        $(this).closest('.shop-menu_cat,.shop-menu_subcat').removeClass('active');
        $(this).closest('.shop-submenu').css({opacity:0},400);
    })
    
    $('.prod-images_thumb').click(function(){
        $('.prod-images_thumb.active').removeClass('active');
        var new_img=$(this).addClass('active').find('img').attr('src');
        $('.prod-images_fullimg').attr('src',new_img);
    })
    
    new Ya.share({
        element: 'yashare',
        elementStyle: {
            'type': 'none',
            'quickServices': ['gplus','twitter','facebook','vkontakte','moimir','yaru','lj','odnoklassniki']
        },
        theme: 'none',
        l10n: 'ru',
        onready: function(){
            $('#ya_share_style').remove(); //убрать стили яндекса
        }
    });
    

})